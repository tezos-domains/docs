# Ghostnet

These contracts are deployed on Ghostnet and correspond to the [Ghostnet dApp instance](https://ghostnet.tezos.domains).

| Contract                             | Address                                      | BCD                                                                          |
| ------------------------------------ | -------------------------------------------- | ---------------------------------------------------------------------------- |
| **NameRegistry.CheckAddress**        | `KT1B3j3At2XMF5P8bVoPD2WeJbZ9eaPiu3pD`       | [🔗](https://better-call.dev/ghostnet/KT1B3j3At2XMF5P8bVoPD2WeJbZ9eaPiu3pD) |
| **NameRegistry.SetChildRecord**      | `KT1HpddfW7rX5aT2cTdsDaQZnH46bU7jQSTU`       | [🔗](https://better-call.dev/ghostnet/KT1HpddfW7rX5aT2cTdsDaQZnH46bU7jQSTU) |
| **NameRegistry.UpdateRecord**        | `KT1Ln4t64RdCG1bK8zkH6Xi4nNQVxz7qNgyj`       | [🔗](https://better-call.dev/ghostnet/KT1Ln4t64RdCG1bK8zkH6Xi4nNQVxz7qNgyj) |
| **NameRegistry.ClaimReverseRecord**  | `KT1H19ouy5QwDBchKXcUw1QRFs5ZYyx1ezEJ`       | [🔗](https://better-call.dev/ghostnet/KT1H19ouy5QwDBchKXcUw1QRFs5ZYyx1ezEJ) |
| **NameRegistry.UpdateReverseRecord** | `KT1HDUc2xtPHqWQcjE1WuinTTHajXQN3asdk`       | [🔗](https://better-call.dev/ghostnet/KT1HDUc2xtPHqWQcjE1WuinTTHajXQN3asdk) |
| **NameRegistry**                     | _resolve from **NameRegistry.CheckAddress**_ | [🔗](https://better-call.dev/ghostnet/KT1REqKBXwULnmU6RpZxnRBUgcBmESnXhCWs) |
| **TLDRegistrar.Buy**                 | `KT1Ks7BBTLLjD9PsdCboCL7fYEfq8z1mEvU1`       | [🔗](https://better-call.dev/ghostnet/KT1Ks7BBTLLjD9PsdCboCL7fYEfq8z1mEvU1) |
| **TLDRegistrar.Renew**               | `KT1Bv32pdMYmBJeMa2HsyUQZiC6FNj1dX6VR`       | [🔗](https://better-call.dev/ghostnet/KT1Bv32pdMYmBJeMa2HsyUQZiC6FNj1dX6VR) |
| **TLDRegistrar.Commit**              | `KT1PEnPDgGKyHvaGzWj6VJJYwobToiW2frff`       | [🔗](https://better-call.dev/ghostnet/KT1PEnPDgGKyHvaGzWj6VJJYwobToiW2frff) |
| **TLDRegistrar.Bid**                 | `KT1P3wdbusZK2sj16YXxRViezzWCPXpiE28P`       | [🔗](https://better-call.dev/ghostnet/KT1P3wdbusZK2sj16YXxRViezzWCPXpiE28P) |
| **TLDRegistrar.Withdraw**            | `KT1C7EF4c1pnPW9qcfNRiTPj5tBFMQJtvUhq`       | [🔗](https://better-call.dev/ghostnet/KT1C7EF4c1pnPW9qcfNRiTPj5tBFMQJtvUhq) |
| **TLDRegistrar.Settle**              | `KT1DMNPg3b3fJQpjXULcXjucEXfwq3zGTKGo`       | [🔗](https://better-call.dev/ghostnet/KT1DMNPg3b3fJQpjXULcXjucEXfwq3zGTKGo) |
| **TLDRegistrar**                     | _resolve from **TLDRegistrar.Buy**_          | [🔗](https://better-call.dev/ghostnet/KT1UZmFPpSFWFkma6yGLTJVmkvUjxTaEqXqW) |
| **AffiliateBuyRenew**                | `KT1EPAzYSkjvnwWYKqER6ZXihV7pxu3s1jr3`       | [🔗](https://better-call.dev/ghostnet/KT1EPAzYSkjvnwWYKqER6ZXihV7pxu3s1jr3) |
