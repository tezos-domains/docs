# Table of contents

* [Introduction](README.md)

## Integrating Tezos Domains

* [Client Libraries](integrating-tezos-domains/client-libraries.md)
* [GraphQL](integrating-tezos-domains/graphql/README.md)
  * [Mainnet](integrating-tezos-domains/graphql/mainnet-endpoint.md)

## Smart Contract Design <a href="#design-document" id="design-document"></a>

* [Smart Contract Overview](design-document/smart-contract-overview.md)
* [Top-Level Domain Registrar](design-document/top-level-domain-registrar.md)
* [Ownership Scheme](design-document/ownership-overview.md)
* [Domain Data](design-document/domain-data.md)

## Contract Interoperability <a href="#interoperability" id="interoperability"></a>

* [Proxy Contracts](interoperability/proxy-contracts.md)
* [Name Resolution](interoperability/name-resolution.md)
* [Buys & Renewals](interoperability/buys-and-renewals.md)
* [Auction Operations](interoperability/auction-operations.md)
* [Domain Operations](interoperability/domain-operations.md)
* [.tez TLD](interoperability/.tez-tld.md)
* [Domains as NFTs](interoperability/domains-as-nfts.md)
* [Affiliated Buys & Renewals](interoperability/affiliated-buys-and-renewals.md)

## Deployed Contracts

* [Mainnet](deployed-contracts/mainnet.md)
* [Ghostnet](deployed-contracts/ghostnet.md)

## Audits

* [Audit by Quantstamp](https://certificate.quantstamp.com/full/tezos-domains)
* [Audit by Inference](https://github.com/InferenceAG/ReportPublications/blob/master/Inference%20AG%20-%20Tezos%20Domains%20-%20Governance_Token_Vesting%20-%20v1.0.pdf)
