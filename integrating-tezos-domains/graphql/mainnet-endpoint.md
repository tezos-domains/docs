# Mainnet

{% swagger baseUrl="https://api.tezos.domains" path="/graphql" method="post" summary="GraphQL" %}
{% swagger-description %}
This endpoint allows you to query Tezos Domains data.
{% endswagger-description %}

{% swagger-parameter in="body" name="" type="object" %}
GraphQL query object
{% endswagger-parameter %}

{% swagger-response status="200" description="Tezos Domains data successfully retrieved." %}
```
{
  "data": {
    "domains": {
      "items": [
        {
          "address": "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
          "name": "aaa.tez",
          "owner": "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
          "level": 2
        },
        {
          "address": null,
          "name": "a.aaa.tez",
          "owner": "tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n",
          "level": 3
        },
        {
          "address": null,
          "name": "alice.tez",
          "owner": "tz1Q4vimV3wsfp21o7Annt64X7Hs6MXg9Wix",
          "level": 2
        }
      ]
    }
  },
  "extensions": {}
}
```
{% endswagger-response %}

{% swagger-response status="429" description="You are hitting the rate limits of Tezos Domains endpoint." %}
```
Status Code: 429
Retry-After: 58
Content: API calls quota exceeded! maximum admitted 100 per 1m.
```
{% endswagger-response %}
{% endswagger %}

### Examples:

#### Get addresses for domains:

```
{
   domains(where: { name: { in: ["domains.tez", "registry.domains.tez"] } }) {
    items {
      address
      owner
      name
      level
    }
  }
}
```

#### Get reverse records for addresses:

```
{
  reverseRecords(
    where: {
      address: {
        in: [
          "KT1Mqx5meQbhufngJnUAGEGpa4ZRxhPSiCgB"
          "KT1GBZmSxmnKJXGMdMLbugPfLyUPmuLSMwKS"
        ]
      }
    }
  ) {
    items {
      address
      owner
      domain {
        name
      }
    }
  }
}
```
