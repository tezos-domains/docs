# Affiliated Buys & Renewals

Tezos Domains introduces an affiliate model that allows partners to offer domain purchases directly through their own apps. Affiliates earn a percentage with each tracked sale. This model is ideal for integration into wallets, marketplaces, and explorers, though any Tezos project can become an affiliate partner. To join the program, please reach out to us via [Discord](https://tezos.domains/discord).

Affiliate purchases are tracked using a dedicated smart contract. This contract extends the standard [buy and renewal](buys-and-renewals.md) entrypoints by including an additional `affiliate` parameter, which identifies the affiliate partner by their Tezos address. You can find the mainnet address in our [deployed contracts list](../deployed-contracts/mainnet.md).

## Contract: AffiliateBuyRenew

### Entrypoint: buy

Buys a second-level domain based on previous commitment (see [TLDRegistrar.Commit](buys-and-renewals.md#contract-tldregistrar-commit)).

**Amount restriction**: The amount sent with this call has to be equal to the price of the domain. The price in mutez is calculated as `standard_price_per_day * duration / 1000000`.

| Parameter     | Type                  | Description                                                               |
| ------------- | --------------------- | ------------------------------------------------------------------------- |
| **label**     | `bytes`               | The UTF-8 encoded label of the second-level domain to buy.                |
| **duration**  | `nat`                 | Ownership duration represented in days.                                   |
| **owner**     | `address`             | The new owner of the given domain.                                        |
| **address**   | `address option`      | The optional address the given domain resolves to.                        |
| **data**      | `(string, bytes) map` | A map of any additional data clients wish to store with the given domain. |
| **nonce**     | `nat`                 | The chosen commitment nonce.                                              |
| **affiliate** | `address`             | Address uniquely identifying the affiliate.                               |

**Errors:**

| Error                        | Description                                                                                                                     |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| COMMITMENT\_DOES\_NOT\_EXIST | Corresponding commitment (see [TLDRegistrar.Commit](buys-and-renewals.md#contract-tldregistrar-commit)) was not created before. |
| COMMITMENT\_TOO\_OLD         | The commitment is too old (older than configured age). Try recreating it again.                                                 |
| COMMITMENT\_TOO\_RECENT      | The commitment is too recent (younger than configured age). Wait for some time.                                                 |
| LABEL\_TAKEN                 | The requested **label** already exists and it is not expired.                                                                   |
| LABEL\_NOT\_AVAILABLE        | The requested **label** is currently not available for registration.                                                            |
| LABEL\_IN\_AUCTION           | The requested **label** is currently only available in auction.                                                                 |
| INVALID\_LABEL               | The given **label** is not valid. See [Label Validation](domain-operations.md#label-validation).                                |
| LABEL\_EMPTY                 | The given label is empty.                                                                                                       |
| LABEL\_TOO\_LONG             | The label is too long.                                                                                                          |
| NAME\_TOO\_LONG              | The name (label + parent) is too long.                                                                                          |
| DURATION\_TOO\_LOW           | The requested **duration** is too low (lower than the configured minimum).                                                      |
| AMOUNT\_TOO\_LOW             | The transferred **amount** is lower than the actual price.                                                                      |
| AMOUNT\_TOO\_HIGH            | The transferred **amount** is higher than the actual price.                                                                     |

### Entrypoint: renew

Renews second-level domain for requested duration.

**Amount restriction**: The amount sent with this call has to be equal to the price of the domain. The price in mutez is calculated as `standard_price_per_day * duration / 1000000`.

| Parameter     | Type      | Description                                                |
| ------------- | --------- | ---------------------------------------------------------- |
| **label**     | `bytes`   | The UTF-8 encoded label of the second-level domain to buy. |
| **duration**  | `nat`     | The renewal duration represented in days.                  |
| **affiliate** | `address` | Address uniquely identifying the affiliate.                |

**Errors:**

| Error              | Description                                                                                |
| ------------------ | ------------------------------------------------------------------------------------------ |
| LABEL\_NOT\_FOUND  | The requested **label** does not exist.                                                    |
| LABEL\_EXPIRED     | The requested **label** exists but it is expired. Therefore it can be bought, not renewed. |
| DURATION\_TOO\_LOW | The specified **duration** is too low (lower than the configured minimum).                 |
| AMOUNT\_TOO\_LOW   | The transferred **amount** is lower than the actual price.                                 |
| AMOUNT\_TOO\_HIGH  | The transferred **amount** is higher than the actual price.                                |

